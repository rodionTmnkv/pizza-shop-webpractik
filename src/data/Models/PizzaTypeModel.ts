import { makeAutoObservable } from 'mobx';
import { FC, SVGProps } from 'react';

export interface IPizzaTypeModel {
    id: number;
    name: string;
    Icon: FC<SVGProps<SVGSVGElement>>;
}

export class PizzaTypeModel implements IPizzaTypeModel {
    public id: number;

    public name: string;

    public Icon: FC<SVGProps<SVGSVGElement>>;

    constructor(id: number, name: string, Icon: FC<SVGProps<SVGSVGElement>>) {
        makeAutoObservable(this);

        this.id = id;
        this.name = name;
        this.Icon = Icon;
    }
}
