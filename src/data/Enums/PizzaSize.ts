// eslint-disable-next-line no-shadow
export enum PizzaSize {
    Small = 20,
    Medium = 30,
    Large = 40,
}
