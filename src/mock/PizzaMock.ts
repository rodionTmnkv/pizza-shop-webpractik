import { PizzaModel } from '@/data/Models/PizzaModel';

import { PizzaSize } from '../data/Enums/PizzaSize';
import { pizzaTypesMock } from './PizzaTypesMock';

export const pizzasMock: PizzaModel[] = [
    {
        id: 1,
        name: 'Итальянская',
        image: '/images/png/main-menu-pizza_1.png',
        description: 'Томат, шампиньон, сыр, оливки, чили, соус, тесто, базилик',
        prices: [
            {
                id: 1,
                size: PizzaSize.Small,
                price: 399,
            },
            {
                id: 2,
                size: PizzaSize.Medium,
                price: 479,
            },
            {
                id: 3,
                size: PizzaSize.Large,
                price: 699,
            },
        ],
        types: [pizzaTypesMock[0]],
    },
    {
        id: 2,
        name: 'Маргарита',
        image: '/images/png/main-menu-pizza_2.png',
        description: 'Тесто со шпинатом, молодой сыр и колбаски, много колбасок',
        prices: [
            {
                id: 1,
                size: PizzaSize.Small,
                price: 399,
            },
            {
                id: 2,
                size: PizzaSize.Medium,
                price: 479,
            },
            {
                id: 3,
                size: PizzaSize.Large,
                price: 699,
            },
        ],
        types: [pizzaTypesMock[1]],
    },
    {
        id: 3,
        name: 'Барбекю',
        image: '/images/png/main-menu-pizza_3.png',
        description: 'Циплёнок (маленький кура), оливки, моцарелла, соус барбекю',
        prices: [
            {
                id: 1,
                size: PizzaSize.Small,
                price: 399,
            },
            {
                id: 2,
                size: PizzaSize.Medium,
                price: 479,
            },
            {
                id: 3,
                size: PizzaSize.Large,
                price: 699,
            },
        ],
        types: [pizzaTypesMock[1]],
    },
    {
        id: 4,
        name: 'Вегетарианская',
        image: '/images/png/main-menu-pizza_4.png',
        description: 'Томат, шампиньон, сыр, оливки, чили, соус, тесто, базилик',
        prices: [
            {
                id: 1,
                size: PizzaSize.Small,
                price: 399,
            },
            {
                id: 2,
                size: PizzaSize.Medium,
                price: 479,
            },
            {
                id: 3,
                size: PizzaSize.Large,
                price: 699,
            },
        ],
        types: [pizzaTypesMock[3]],
    },
    {
        id: 5,
        name: 'Мясная',
        image: '/images/png/main-menu-pizza_5.png',
        description: 'Томат, шампиньон, сыр, оливки, чили, соус, тесто, базилик',
        prices: [
            {
                id: 1,
                size: PizzaSize.Small,
                price: 399,
            },
            {
                id: 2,
                size: PizzaSize.Medium,
                price: 479,
            },
            {
                id: 3,
                size: PizzaSize.Large,
                price: 699,
            },
        ],
        types: [pizzaTypesMock[1], pizzaTypesMock[0]],
    },
    {
        id: 6,
        name: 'Овощная',
        image: '/images/png/main-menu-pizza_6.png',
        description: 'Тесто со шпинатом, молодой сыр и колбаски, много колбасок',
        prices: [
            {
                id: 1,
                size: PizzaSize.Small,
                price: 399,
            },
            {
                id: 2,
                size: PizzaSize.Medium,
                price: 479,
            },
            {
                id: 3,
                size: PizzaSize.Large,
                price: 699,
            },
        ],
        types: [pizzaTypesMock[3]],
    },
    {
        id: 7,
        name: 'Римская',
        image: '/images/png/main-menu-pizza_7.png',
        description: 'Циплёнок (маленький кура), оливки, моцарелла, соус барбекю',
        prices: [
            {
                id: 1,
                size: PizzaSize.Small,
                price: 399,
            },
            {
                id: 2,
                size: PizzaSize.Medium,
                price: 479,
            },
            {
                id: 3,
                size: PizzaSize.Large,
                price: 699,
            },
        ],
        types: [pizzaTypesMock[0]],
    },
    {
        id: 8,
        name: 'С грибами',
        image: '/images/png/main-menu-pizza_8.png',
        description: 'Томат, шампиньон, сыр, оливки, чили, соус, тесто, базилик',
        prices: [
            {
                id: 1,
                size: PizzaSize.Small,
                price: 399,
            },
            {
                id: 2,
                size: PizzaSize.Medium,
                price: 479,
            },
            {
                id: 3,
                size: PizzaSize.Large,
                price: 699,
            },
        ],
        types: [pizzaTypesMock[2]],
    },
    {
        id: 9,
        name: 'Сырная',
        image: '/images/png/main-menu-pizza_9.png',
        description: 'Томат, шампиньон, сыр, оливки, чили, соус, тесто, базилик',
        prices: [
            {
                id: 1,
                size: PizzaSize.Small,
                price: 399,
            },
            {
                id: 2,
                size: PizzaSize.Medium,
                price: 479,
            },
            {
                id: 3,
                size: PizzaSize.Large,
                price: 699,
            },
        ],
        types: [pizzaTypesMock[2]],
    },
    {
        id: 10,
        name: 'Четыре сыра',
        image: '/images/png/main-menu-pizza_10.png',
        description: 'Тесто со шпинатом, молодой сыр и колбаски, много колбасок',
        prices: [
            {
                id: 1,
                size: PizzaSize.Small,
                price: 399,
            },
            {
                id: 2,
                size: PizzaSize.Medium,
                price: 479,
            },
            {
                id: 3,
                size: PizzaSize.Large,
                price: 699,
            },
        ],
        types: [pizzaTypesMock[2]],
    },
    {
        id: 11,
        name: 'Пепперони Фреш с томатами',
        image: '/images/png/main-menu-pizza_11.png',
        description: 'Циплёнок (маленький кура), оливки, моцарелла, соус барбекю',
        prices: [
            {
                id: 1,
                size: PizzaSize.Small,
                price: 399,
            },
            {
                id: 2,
                size: PizzaSize.Medium,
                price: 479,
            },
            {
                id: 3,
                size: PizzaSize.Large,
                price: 699,
            },
        ],
        types: [],
    },
    {
        id: 12,
        name: 'Ветчина и сыр',
        image: '/images/png/main-menu-pizza_12.png',
        description: 'Томат, шампиньон, сыр, оливки, чили, соус, тесто, базилик',
        prices: [
            {
                id: 1,
                size: PizzaSize.Small,
                price: 399,
            },
            {
                id: 2,
                size: PizzaSize.Medium,
                price: 479,
            },
            {
                id: 3,
                size: PizzaSize.Large,
                price: 699,
            },
        ],
        types: [pizzaTypesMock[1]],
    },
];
