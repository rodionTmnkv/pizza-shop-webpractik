import AcutePizzaIcon from '@/components/UI/svgComponents/pizzaTypeIcons/AcutePizzaIcon';
import CheesePizzaIcon from '@/components/UI/svgComponents/pizzaTypeIcons/CheesePizzaIcon';
import MeatPizzaIcon from '@/components/UI/svgComponents/pizzaTypeIcons/MeatPizzaIcon';
import VeganPizzaIcon from '@/components/UI/svgComponents/pizzaTypeIcons/VeganPizzaIcon';

import { PizzaTypeModel } from '../data/Models/PizzaTypeModel';

export const pizzaTypesMock: PizzaTypeModel[] = [
    {
        id: 1,
        name: 'Острые',
        Icon: AcutePizzaIcon,
    },
    {
        id: 2,
        name: 'Мясные',
        Icon: MeatPizzaIcon,
    },
    {
        id: 3,
        name: 'Сырные',
        Icon: CheesePizzaIcon,
    },
    {
        id: 4,
        name: 'Веганские',
        Icon: VeganPizzaIcon,
    },
];
