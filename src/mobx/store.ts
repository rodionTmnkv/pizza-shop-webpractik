import { inject } from 'mobx-react';

import { CartModel } from '@/data/Models/CartModel';
import { PizzaFilterModel } from '@/data/Models/PizzaFilterModel';
import { PizzaModel } from '@/data/Models/PizzaModel';
import { PizzaTypeModel } from '@/data/Models/PizzaTypeModel';
import { pizzasMock } from '@/mock/PizzaMock';
import { pizzaTypesMock } from '@/mock/PizzaTypesMock';

export interface IStore {
    pizzas: PizzaModel[];
    pizzaTypes: PizzaTypeModel[];
    pizzaFilter: PizzaFilterModel;
    cart: CartModel;
}

export const store: IStore = {
    pizzas: pizzasMock,
    pizzaTypes: pizzaTypesMock,
    pizzaFilter: new PizzaFilterModel(null),
    cart: new CartModel([]),
};

export const typedInject = (key: keyof IStore) => inject(key);
