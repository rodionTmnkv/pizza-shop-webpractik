import Head from 'next/head';
import React, { useState } from 'react';

import { Cart } from '@/components/blocks/Modals/Cart';
import Footer from '@/components/layouts/Footer';
import Header from '@/components/layouts/Header';
import Main from '@/components/layouts/Main';

const IndexPage = () => {
    const [needShowCart, setNeedShowCart] = useState(false);
    return (
        <>
            <Head>
                <title>Pizza Shop</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Cart isOpen={needShowCart} onClose={() => setNeedShowCart(false)} />
            <Header showCart={() => setNeedShowCart(true)} />
            <Main />
            <Footer />
        </>
    );
};

export default IndexPage;
