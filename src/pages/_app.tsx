import 'normalize.css';
import '@fontsource/alegreya/900.css';
import '@fontsource/alegreya/800.css';
import '@fontsource/alegreya/400.css';
import '@fontsource/roboto/900.css';
import '@fontsource/roboto/700.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/400.css';
import '@/assets/index.sass';

import { Provider } from 'mobx-react';
import React from 'react';

import { StoreProvider } from '@/hooks/useStore';
import { store } from '@/mobx/store';

const App = ({ Component, pageProps }) => (
    <StoreProvider {...pageProps}>
        <Provider {...store}>
            <Component {...pageProps} />
        </Provider>
    </StoreProvider>
);

export const reportWebVitals = (metrics: { label: string; name: string; value: number }) => {
    if (metrics.label === 'web-vital') {
        console.log({ name: metrics.name, value: Math.trunc(metrics.value) });
    }
};

export default App;
