import clsx from 'classnames';
import React, { ReactNode } from 'react';

import cn from './style.module.sass';

type Props = {
    children: string | ReactNode;
    type?: 'submit' | 'reset' | 'button';
    classname: string;
    onClick?: () => void;
};

const ButtonComponent = ({ children, type = 'button', classname, onClick }: Props) => (
    <button type={type} onClick={onClick} className={clsx(cn['button-component'], classname)}>
        {children}
    </button>
);

export default ButtonComponent;
