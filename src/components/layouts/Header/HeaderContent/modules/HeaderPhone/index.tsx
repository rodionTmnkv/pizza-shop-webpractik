import React from 'react';

import HeaderContentTelIcon from '@/components/UI/svgComponents/HeaderContentTelIcon';

// Ипорт стилей
import cn from './styles.module.sass';

type Props = {
    scrolled: boolean;
};

const HeaderPhone = ({ scrolled }: Props): React.ReactElement => (
    <div className={`${cn['header-phone']} ${scrolled ? cn['header-phone--scrolled'] : ''}`}>
        <HeaderContentTelIcon
            className={`${cn['header-phone__image']} ${
                scrolled ? cn['header-phone__image--scrolled'] : ''
            }`}
        />
        <div className={cn['header-phone__contacts']}>
            <a
                className={`${cn['header-phone-contacts__tel']} ${
                    scrolled ? cn['header-phone-contacts__tel--scrolled'] : ''
                }`}
                href="tel:+7 ( 918 ) 432 - 65 - 87"
            >
                +7 ( 918 ) 432 - 65 - 87
            </a>
            <span className={`${cn.description} ${scrolled ? cn['description--scrolled'] : ''}`}>
                Ежедневно с 9:00 до 23:00
            </span>
        </div>
    </div>
);

export default HeaderPhone;
