import React from 'react';

import HeaderContentCartIcon from '@/components/UI/svgComponents/HeaderContentCartIcon';
import { withStateManager } from '@/hock/withStateManager';
import { IStore } from '@/mobx/store';

// Ипорт стилей
import cn from './styles.module.sass';

type Props = {
    showCart: () => void;
    scrolled: boolean;
};

type InjectedProps = Pick<IStore, 'cart'>;

const HeaderOrder = ({ showCart, cart, scrolled }: Props & InjectedProps): React.ReactElement => (
    <div aria-hidden="true" className={cn['header-order']} onClick={showCart}>
        <span
            className={`${cn['header-order__counter']} ${
                scrolled ? cn['header-order__counter--scrolled'] : ''
            }`}
        >
            {cart.getItemsCount()}
        </span>
        <HeaderContentCartIcon
            className={`${cn['header-order__image']} ${
                scrolled ? cn['header-order__image--scrolled'] : ''
            }`}
        />
        <div
            className={`${cn['header-order__info']} ${
                scrolled ? cn['header-order__info--scrolled'] : ''
            }`}
        >
            <span
                className={`${cn['header-order__title']} ${
                    scrolled ? cn['header-order__title--scrolled'] : ''
                }`}
            >
                Ваш заказ
            </span>
            <span className={`${cn.description} ${scrolled ? cn['description--scrolled'] : ''}`}>
                {cart.getOrderName()}
            </span>
        </div>
    </div>
);

export default withStateManager<Props, InjectedProps>(HeaderOrder, stores => ({
    cart: stores.cart,
}));
