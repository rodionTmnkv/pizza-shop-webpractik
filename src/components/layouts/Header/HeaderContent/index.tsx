import React from 'react';

import HeaderOrder from '@/components/layouts/Header/HeaderContent/modules/HeaderOrder';
import HeaderPhone from '@/components/layouts/Header/HeaderContent/modules/HeaderPhone';

import HeaderMobileNav from '../HeaderMobileNav';
// Ипорт стилей
import cn from './styles.module.sass';

type Props = {
    showCart: () => void;
    scrolled: boolean;
};

const HeaderContent = ({ showCart, scrolled }: Props): React.ReactElement => (
    <>
        <div className={cn['header-container']}>
            <HeaderPhone scrolled={scrolled} />
            <HeaderOrder scrolled={scrolled} showCart={showCart} />
        </div>
        <button
            type="button"
            className={`${cn['header-lang']} ${scrolled ? cn['header-lang--scrolled'] : ''}`}
        >
            EN
        </button>
        <HeaderMobileNav />
    </>
);

export default HeaderContent;
