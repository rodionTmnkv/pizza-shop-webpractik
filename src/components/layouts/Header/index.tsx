import classnames from 'classnames';
import React from 'react';
import { withStateManager } from 'src/hock/withStateManager';
import { IStore } from 'src/mobx/store';

import useScroll from '@/hooks/useScroll';

import HeaderContent from './HeaderContent';
// Импорт компонентов
import HeaderLogo from './HeaderLogo';
import HeaderNav from './HeaderNav';
// Ипорт стилей
import cn from './styles.module.sass';

type Props = {
    showCart: () => void;
};

type InjectedProps = Pick<IStore, 'cart'>;

const Header = ({ showCart }: Props & InjectedProps): React.ReactElement => {
    const scrolled = useScroll();
    return (
        <header
            className={classnames(cn.header, {
                [cn['header-scroll']]: scrolled,
            })}
            id="header"
        >
            <div className={cn.inner}>
                <HeaderLogo scrolled={scrolled} />
                <HeaderNav scrolled={scrolled} />
                <div className={cn.header_wrapper}>
                    <HeaderContent scrolled={scrolled} showCart={showCart} />
                </div>
            </div>
        </header>
    );
};

export default withStateManager<Props, InjectedProps>(Header, stores => ({
    cart: stores.cart,
}));
