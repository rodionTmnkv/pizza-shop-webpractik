// Импорт компонентов
import Link from 'next/link';
import React from 'react';

import HeaderLogoIcon from '@/components/UI/svgComponents/HeaderLogoIcon';

// Ипорт стилей
import cn from './styles.module.sass';

const HeaderLogo = ({ scrolled }: { scrolled: boolean }): React.ReactElement => (
    <Link href="#">
        <HeaderLogoIcon
            className={`${cn['header-logo']} ${scrolled ? cn['header-logo--scrolled'] : ''}`}
        />
    </Link>
);

export default HeaderLogo;
