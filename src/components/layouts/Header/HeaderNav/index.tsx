// Ипорт стилей
import React, { useState } from 'react';
import { Link } from 'react-scroll';

import cn from './styles.module.sass';

const HeaderNav = ({ scrolled }): React.ReactElement => {
    const [visibility, setVisibility] = useState(false);

    return (
        <div
            className={`${cn['header-nav']} ${visibility ? cn.open : ''} && ${
                scrolled ? cn['header-nav--scrolled'] : ''
            }`}
            id="header-navbar"
            role="presentation"
            onClick={() => setVisibility(false)}
            onKeyDown={() => setVisibility(false)}
            onKeyUp={() => setVisibility(false)}
            onFocus={() => setVisibility(false)}
        >
            <Link to="menu" duration={300} className={cn['header-nav__item']}>
                Меню
            </Link>
            <Link to="benefits" duration={200} className={cn['header-nav__item']}>
                О нас
            </Link>
            <Link to="inst" duration={200} className={cn['header-nav__item']}>
                Контакты
            </Link>
        </div>
    );
};

export default HeaderNav;
