// Ипорт стилей
import React, { useState } from 'react';
import { Link } from 'react-scroll';

import HeaderContentButtonIcon from '@/components/UI/svgComponents/HeaderContentButtonIcon';
import HeaderLogoIcon from '@/components/UI/svgComponents/HeaderLogoIcon';
import HeaderMobileCloseIcon from '@/components/UI/svgComponents/HeaderMobileCloseIcon';

import cn from './styles.module.sass';

// Импорт компонентов

const HeaderMobileNav = (): React.ReactElement => {
    const [isMenuOpen, setIsMenuOpen] = useState(false);

    const openMenu = () => {
        const body = document.getElementsByTagName('body')[0];
        body.style.overflow = 'hidden';
        setIsMenuOpen(true);
    };

    const hideMenu = () => {
        const body = document.getElementsByTagName('body')[0];
        body.attributes.removeNamedItem('style');
        setIsMenuOpen(false);
    };

    return (
        <div className={cn['mobile-nav']}>
            <div className={`${cn['mobile-nav__menu']} ${isMenuOpen ? cn.active : ''}`}>
                <div className={cn['mobile-nav__container']}>
                    <div className={cn['mobile-nav__title']}>
                        <HeaderLogoIcon className={cn['mobile-nav__logo']} />
                        <HeaderMobileCloseIcon
                            className={cn['mobile-nav__close']}
                            onClick={hideMenu}
                        />
                    </div>
                    <nav className={cn['mobile-nav__list']}>
                        <div className={cn['mobile-navListBlock']}>
                            <Link
                                to="menu"
                                duration={300}
                                className={cn['mobile-nav__link']}
                                onClick={hideMenu}
                                offset={-60}
                            >
                                Меню
                            </Link>
                        </div>

                        <div className={cn['mobile-navListBlock']}>
                            <Link
                                to="benefits"
                                duration={200}
                                className={cn['mobile-nav__link']}
                                onClick={hideMenu}
                                offset={-60}
                            >
                                О нас
                            </Link>
                        </div>
                        <div className={cn['mobile-navListBlock']}>
                            <Link
                                to="inst"
                                duration={200}
                                className={cn['mobile-nav__link']}
                                onClick={hideMenu}
                                offset={-60}
                            >
                                Контакты
                            </Link>
                        </div>
                    </nav>
                </div>
                <div className={cn['mobile-nav__contacts']}>
                    <span className={cn['mobile-nav__text']}>Заказать по телефону</span>
                    <a className={cn['mobile-nav__tel']} href="tel:+7 ( 918 ) 432 - 65 - 87">
                        +7 ( 918 ) 432 - 65 - 87
                    </a>
                    <span className={cn['mobile-nav__description']}>Ежедневно с 9:00 до 23:00</span>
                    <a href="#" className={cn['mobile-nav__lang']}>
                        English
                    </a>
                </div>
            </div>
            <button
                className={cn['mobile-nav__button']}
                onClick={openMenu}
                onKeyDown={openMenu}
                onKeyUp={openMenu}
                role="menu"
                type="button"
            >
                <HeaderContentButtonIcon className={cn['mobile-nav__btn']} />
            </button>
        </div>
    );
};

export default HeaderMobileNav;
