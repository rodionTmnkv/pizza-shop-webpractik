import Link from 'next/link';
import React from 'react';

import HeaderLogoIcon from '@/components/UI/svgComponents/HeaderLogoIcon';

// Ипорт стилей
import cn from './styles.module.sass';

const Footer = (): React.ReactElement => (
    <footer className={cn.footer}>
        <div className={cn.inner}>
            <div className={cn['footer-container']}>
                <HeaderLogoIcon className={cn['footer-container__logo']} />

                <div className={cn['footer-container__phone']}>
                    <a className={cn['footer-container__tel']} href="tel:+7 ( 918 ) 432 - 65 - 87">
                        +7 ( 918 ) 432 - 65 - 87
                    </a>
                    <span className={cn['footer-container__description']}>
                        Ежедневно с 9:00 до 23:00
                    </span>
                </div>
            </div>
            <span className={cn['footer-policy']}>
                <Link href="/#">
                    <a>Политика конфиденциальности</a>
                </Link>
            </span>
        </div>
    </footer>
);

export default Footer;
