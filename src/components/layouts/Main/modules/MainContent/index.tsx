import Image from 'next/image';
import React from 'react';
import { Link } from 'react-scroll';

import ButtonComponent from '@/components/UI/buttonComponent';

// Импорт стилей
import cn from './styles.module.sass';

const MainContent = (): React.ReactElement => (
    <div className={cn.inner}>
        <div className={cn.mainContent}>
            <div className={cn.mainContentTitle}>
                <div className={cn.mainContentTitleHeading}>Пицца на заказ</div>
                <span className={cn.mainContentTitleAccent}>
                    Бесплатная и быстрая доставка за час в любое удобное для вас время
                </span>
                <Link to="menu" duration={400}>
                    <ButtonComponent classname={cn.mainContentButton} type="button">
                        Выбрать пиццу
                    </ButtonComponent>
                </Link>
            </div>
            <div className={cn.mainContentWrapper}>
                <div className={cn.mainContentWrapperBlock}>
                    <Image
                        className={cn.mainContentWrapperImage}
                        src="/images/png/main-content.png"
                        alt=""
                        layout="responsive"
                        width={1007}
                        height={630}
                    />
                </div>
            </div>
        </div>
    </div>
);

export default MainContent;
