import Image from 'next/image';
import React from 'react';

// Импорт стилей
import cn from './styles.module.sass';

const MainInst = (): React.ReactElement => (
    <div className={cn['main-inst']} id="inst">
        <span className={cn['main-inst-title']}>Следите за нами в Instagram</span>
        <span className={cn['main-inst-link']}>
            <a href="#">@pizzamenu</a>
        </span>
        <div className={cn['main-inst-pictures']}>
            <div className={cn['main-inst-pictures__image']}>
                <Image src="/images/png/main-inst01.png" alt="" width={500} height={500} />
            </div>
            <div className={cn['main-inst-pictures__image']}>
                <Image src="/images/png/main-inst02.png" alt="" width={500} height={500} />
            </div>
            <div className={cn['main-inst-pictures__image']}>
                <Image src="/images/png/main-inst03.png" alt="" width={500} height={500} />
            </div>
            <div className={cn['main-inst-pictures__image']}>
                <Image src="/images/png/main-inst04.png" alt="" width={500} height={500} />
            </div>
            <div className={cn['main-inst-pictures__image']}>
                <Image src="/images/png/main-inst05.png" alt="" width={500} height={500} />
            </div>
            <div className={cn['main-inst-pictures__image']}>
                <Image src="/images/png/main-inst06.png" alt="" width={500} height={500} />
            </div>
            <div className={cn['main-inst-pictures__image']}>
                <Image src="/images/png/main-inst07.png" alt="" width={500} height={500} />
            </div>
            <div className={cn['main-inst-pictures__image']}>
                <Image src="/images/png/main-inst08.png" alt="" width={500} height={500} />
            </div>
            <div className={cn['main-inst-pictures__image']}>
                <Image src="/images/png/main-inst09.png" alt="" width={500} height={500} />
            </div>
            <div className={cn['main-inst-pictures__image']}>
                <Image src="/images/png/main-inst10.png" alt="" width={500} height={500} />
            </div>
        </div>
    </div>
);

export default MainInst;
