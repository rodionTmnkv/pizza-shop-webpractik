import Image from 'next/image';
import React from 'react';

// Импорт стилей
import cn from './styles.module.sass';

const stocksList = [
    {
        img: '/images/svg/stocks/pizza.svg',
        title: 'Закажи 2 пиццы – 3-я в подарок',
        description: 'При заказе 2-х больших пицц – средняя пицца в подарок',
    },
    {
        img: '/images/svg/stocks/drink.svg',
        title: 'Напиток в подарок',
        description: 'Скидка на заказ от 3 000 рублей + напиток в подарок',
    },
    {
        img: '/images/svg/stocks/discount.svg',
        title: '25% при первом заказе',
        description: 'Скидка новым клиентам!',
    },
];

const MainStocks = (): React.ReactElement => (
    <div className={cn.mainStocks}>
        <div className={cn.inner}>
            {stocksList.map((item, index) => (
                <a className={cn.mainStocksItem} key={index} target="_blank" href="#">
                    <div className={cn.mainStocksWrapper}>
                        <Image
                            src={item.img}
                            alt="stocks"
                            layout="responsive"
                            width={416}
                            height={247}
                            objectFit="contain"
                        />
                    </div>
                    <div className={cn.mainStocksContainer}>
                        <span className={cn.mainStocksTitle}>{item.title}</span>
                        <span className={cn.mainStocksDescription}>{item.description}</span>
                    </div>
                </a>
            ))}
        </div>
    </div>
);

export default MainStocks;
