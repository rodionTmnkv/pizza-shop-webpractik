import { observer } from 'mobx-react';
import React from 'react';

import DeliveryIcon from '@/components/UI/svgComponents/deliveryIcons/DeliveryIcon';
import OrderIcon from '@/components/UI/svgComponents/deliveryIcons/OrderIcon';
import PaymentIcon from '@/components/UI/svgComponents/deliveryIcons/PaymentIcon';

// Импорт стилей
import cn from './styles.module.sass';

const MainDelivery = (): React.ReactElement => (
    <div className={cn['main-delivery']}>
        <div className={cn.inner}>
            <div className={cn['main-delivery__title']}>Доставка и оплата</div>
            <div className={cn['main-delivery__wrapper']}>
                <div className={cn['main-delivery__container']}>
                    <div className={cn['main-delivery__svg']}>
                        <OrderIcon />
                    </div>
                    <span className={cn['main-delivery__container__services']}>
                        <span className={cn['main-delivery__container__title']}>Заказ</span>
                        <span className={cn['main-delivery__container__text']}>
                            После оформления заказа мы свяжемся с вами для уточнения деталей.
                        </span>
                    </span>
                </div>
                <div className={cn['main-delivery__container']}>
                    <div className={cn['main-delivery__svg']}>
                        <DeliveryIcon />
                    </div>
                    <span className={cn['main-delivery__container__services']}>
                        <span className={cn['main-delivery__container__title']}>
                            Доставка курьером
                        </span>
                        <span className={cn['main-delivery__container__text']}>
                            Мы доставим вашу пиццу горячей. Бесплатная доставка по городу.
                        </span>
                    </span>
                </div>
                <div className={cn['main-delivery__container']}>
                    <div className={cn['main-delivery__svg']}>
                        <PaymentIcon />
                    </div>
                    <span className={cn['main-delivery__container__services']}>
                        <span className={cn['main-delivery__container__title']}>Оплата</span>
                        <span className={cn['main-delivery__container__text']}>
                            Оплатить можно наличными или картой курьеру. И золотом тоже можно.
                        </span>
                    </span>
                </div>
            </div>
        </div>
    </div>
);

export default observer(MainDelivery);
