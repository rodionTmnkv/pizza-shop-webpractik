import Image from 'next/image';
import React, { useState } from 'react';
// Импорт компонентов
import { PizzaSize } from 'src/data/Enums/PizzaSize';
import { PizzaModel } from 'src/data/Models/PizzaModel';
import { withStateManager } from 'src/hock/withStateManager';
import { IStore } from 'src/mobx/store';

import ButtonComponent from '@/components/UI/buttonComponent';

// Импорт стилей
import cn from './styles.module.sass';

type Props = {
    pizza: PizzaModel;
};

type InjectedProps = Pick<IStore, 'cart'>;

function MainPizzaCard({ pizza, cart }: Props & InjectedProps): React.ReactElement {
    const [selectedSize, setSelectedSize] = useState(PizzaSize.Medium);

    const selectedPrice = pizza.prices.find(price => price.size === selectedSize)?.price;

    const getPizzaImageClassName = () => {
        switch (selectedSize) {
            case PizzaSize.Small:
                return cn.small;

            case PizzaSize.Medium:
                return cn.medium;

            case PizzaSize.Large:
                return cn.large;

            default:
                throw new Error('Unknown pizza size');
        }
    }; // getPizzaImageClassName

    const handleSizeSelecting = (e: React.ChangeEvent<HTMLInputElement>) => {
        setSelectedSize(Number(e.target.value) as PizzaSize);
    }; // handleSizeSelecting

    const handleOrdering = () => {
        cart.addToCart(pizza, selectedSize);
    }; // handleOrdering

    return (
        <div className={cn['menu-card']}>
            <div className={cn['menu-card-type']}>
                {pizza.types.map(({ Icon, id }) => (
                    <Icon className={cn['menu-card-type__icon']} key={id} />
                ))}
            </div>

            <div className={cn['menu-card-wrapper']}>
                <div className={`${cn['menu-card-wrapper__image']} ${getPizzaImageClassName()}`}>
                    <Image src={pizza.image} alt={pizza.name} layout="fill" />
                </div>
            </div>

            <div className={cn['menu-card-content']}>
                <div className={cn['menu-card-content__info']}>
                    <span className={cn['menu-card-content__title']}>{pizza.name}</span>
                    <span className={cn['menu-card-content__description']}>
                        {pizza.description}
                    </span>
                </div>
                <div className={cn['menu-card-options']}>
                    <span className={cn['menu-card-options__size']}>Размер, см</span>
                    <div className={cn['menu-card-options__buttons']}>
                        {pizza.prices.map(price => (
                            <label
                                key={price.id}
                                className={`${cn['menu-card-options__label']} ${
                                    selectedSize === price.size ? cn.active : ''
                                }`}
                            >
                                <input
                                    className={cn['menu-card-options__button']}
                                    value={price.size}
                                    type="radio"
                                    checked={selectedSize === price.size}
                                    onChange={handleSizeSelecting}
                                />
                                {price.size}
                            </label>
                        ))}
                    </div>

                    <span className={cn['menu-card-price']}>от {selectedPrice} руб</span>

                    <ButtonComponent
                        classname={cn['menu-card-button']}
                        type="button"
                        onClick={handleOrdering}
                    >
                        <span className={cn['menu-card-button__price']}>
                            от {selectedPrice} руб
                        </span>
                        <span className={cn['menu-card-button__text']}>Заказать</span>
                    </ButtonComponent>
                </div>
            </div>
        </div>
    );
}

export default withStateManager<Props, InjectedProps>(MainPizzaCard, stores => ({
    cart: stores.cart,
}));
