import React from 'react';
import { withStateManager } from 'src/hock/withStateManager';
import { IStore } from 'src/mobx/store';

// Импорт компонентов
import MainPizzaCard from './modules/MainPizzaCard';
// Импорт стилей
import cn from './styles.module.sass';

type InjectedProps = Pick<IStore, 'pizzas'>;

const MainPizzasList = ({ pizzas }: InjectedProps): React.ReactElement => (
    <div className={cn['main-menu__cards']}>
        {pizzas.map(pizza => (
            <MainPizzaCard key={pizza.id} pizza={pizza} />
        ))}
    </div>
);

export default withStateManager<unknown, InjectedProps>(MainPizzasList, stores => ({
    pizzas: stores.pizzas.filter(
        p =>
            stores.pizzaFilter.pizzaTypeId == null ||
            p.types.find(t => t.id === stores.pizzaFilter.pizzaTypeId)
    ),
}));
