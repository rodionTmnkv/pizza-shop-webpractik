// Импорт компонентов
import clsx from 'classnames';
import { FC } from 'react';

import AllPizzaIcon from '@/components/UI/svgComponents/pizzaTypeIcons/AllPizzaIcon';
import { IPizzaFilterModel } from '@/data/Models/PizzaFilterModel';
import { IPizzaTypeModel } from '@/data/Models/PizzaTypeModel';
import { withStateManager } from '@/hock/withStateManager';

// Импорт стилей
import cn from './styles.module.sass';

interface InjectedProps {
    pizzaTypes: IPizzaTypeModel[];
    pizzaFilter: IPizzaFilterModel;
}

const MainPizzaTypes: FC<InjectedProps> = ({ pizzaTypes, pizzaFilter }) => (
    <>
        <div className={cn['main-menu__heading']}>Выберите пиццу</div>
        <nav className={cn['main-menu__nav']}>
            <div
                className={clsx(cn['main-menu__nav__content'], {
                    [cn.active]: pizzaFilter.pizzaTypeId === null,
                })}
                role="presentation"
                onClick={() => pizzaFilter?.setPizzaTypeId(null)}
                onKeyDown={() => pizzaFilter?.setPizzaTypeId(null)}
            >
                <span className={cn['main-menu__nav__list']}>Все</span>

                <AllPizzaIcon className={cn['main-menu__nav__svg']} />
            </div>

            {pizzaTypes.map(({ Icon, name, id }) => (
                <div
                    className={clsx(cn['main-menu__nav__content'], {
                        [cn.active]: pizzaFilter.pizzaTypeId === id,
                    })}
                    role="presentation"
                    key={id}
                    onClick={() => pizzaFilter.setPizzaTypeId(id)}
                    onKeyDown={() => pizzaFilter.setPizzaTypeId(id)}
                >
                    <Icon className={cn['main-menu__nav__svg']} />

                    <span className={cn['main-menu__nav__list']}>{name}</span>
                </div>
            ))}
        </nav>
    </>
);

export default withStateManager<unknown, InjectedProps>(MainPizzaTypes, stores => ({
    pizzaTypes: stores.pizzaTypes,
    pizzaFilter: stores.pizzaFilter,
}));
