import React from 'react';

// Импорт компонентов
import MainPizzasList from './modules/MainPizzasList';
import MainPizzaTypes from './modules/MainPizzaTypes';
// Импорт стилей
import cn from './styles.module.sass';

const MainMenu = (): React.ReactElement => (
    <div className={cn.inner}>
        <div className={cn['main-menu']} id="menu">
            <MainPizzaTypes />
            <MainPizzasList />
        </div>
    </div>
);

export default MainMenu;
