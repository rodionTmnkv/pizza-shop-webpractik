// Импорт стилей
import Image from 'next/image';
import React from 'react';

import cn from './styles.module.sass';
// Импорт компонентов

const benefitsList = [
    {
        img: '/images/png/main-benefits-cooking.png',
        title: 'Изготавливаем пиццу по своим рецептам в лучших традициях',
        // eslint-disable-next-line max-len
        text: 'Наша пицца получается сочной, вкусной и главное хрустящей с нежной и аппетитной начинкой, готовим по своим итальянским рецептам',
    },
    {
        img: '/images/png/main-benefits-ingredients.png',
        title: 'Используем только свежие ингридиенты',
        text: 'Ежедневно заготавливаем продукты и овощи для наших пицц, соблюдаем все сроки хранения',
    },
    {
        img: '/images/png/main-benefits-services.png',
        title: 'Доставка в течение 60 минут или заказ за нас счёт',
        // eslint-disable-next-line max-len
        text: 'Все наши курьеры – фанаты серии Need for Speed и призеры гонок World Rally Championship и World Superbike во всех категориях',
    },
];

const MainBenefits = (): React.ReactElement => (
    <div className={cn.mainBenefits} id="benefits">
        <div className={cn.inner}>
            {benefitsList.map((item, index) => (
                <div className={cn.mainBenefitsContainer} key={index}>
                    <div className={cn.mainBenefitsContainerImageWrapper}>
                        <Image
                            src={item.img}
                            alt=""
                            className={cn.mainBenefitsContainerImage}
                            layout="fill"
                            objectFit="contain"
                        />
                    </div>
                    <div className={cn.mainBenefitsContainerInfo}>
                        <div className={cn.mainBenefitsContainerTitle}>{item.title}</div>
                        <div className={cn.mainBenefitsContainerText}>{item.text}</div>
                    </div>
                </div>
            ))}
        </div>
    </div>
);

export default MainBenefits;
