import React from 'react';

import StocksHandleResize from '@/components/blocks/StocksHandleResize';
import MainBenefits from '@/components/layouts/Main/modules/MainBenefits';
// Импорт компонентов
import MainContent from '@/components/layouts/Main/modules/MainContent';
import MainDelivery from '@/components/layouts/Main/modules/MainDelivery';
import MainInst from '@/components/layouts/Main/modules/MainInst';
import MainMenu from '@/components/layouts/Main/modules/MainMenu';

// Импорт стилей
import cn from './styles.module.sass';

const Main = (): React.ReactElement => (
    <main className={cn.main}>
        <MainContent />

        <StocksHandleResize />
        <MainMenu />
        <MainDelivery />
        <MainBenefits />
        <MainInst />
    </main>
);

export default Main;
