import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import Image from 'next/image';
import React from 'react';
import Slider from 'react-slick';

import cn from './styles.module.sass';

const sliderList = [
    {
        img: '/images/svg/stocks/pizza.svg',
        title: 'Закажи 2 пиццы – 3-я в подарок',
        description: 'При заказе 2-х больших пицц – средняя пицца в подарок',
    },
    {
        img: '/images/svg/stocks/drink.svg',
        title: 'Напиток в подарок',
        description: 'Скидка на заказ от 3 000 рублей + напиток в подарок',
    },
    {
        img: '/images/svg/stocks/discount.svg',
        title: '25% при первом заказе',
        description: 'Скидка новым клиентам!',
    },
];

function StocksSlider(): React.ReactElement {
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        arrows: false,
        fade: true,
    };

    return (
        <div className={cn.slickSlider}>
            <div className={cn.slickSliderInner}>
                <Slider {...settings}>
                    {sliderList.map((item, index) => (
                        <div key={index}>
                            <Image
                                src={item.img}
                                alt="stocks"
                                width={288}
                                height={171}
                                layout="responsive"
                                objectFit="contain"
                            />
                            <div className={cn.slickSliderContainer}>
                                <span className={cn.slickSliderTitle}>{item.title}</span>
                                <span className={cn.slickSliderDescription}>
                                    {item.description}
                                </span>
                            </div>
                        </div>
                    ))}
                </Slider>
            </div>
        </div>
    );
}

export default StocksSlider;
