// Импорт стилей
import React, { PropsWithChildren } from 'react';
import ReactModal from 'react-modal';

import ModalCloseIcon from '@/components/UI/svgComponents/ModalCloseIcon';

import cn from './styles.module.sass';

// eslint-disable-next-line @typescript-eslint/no-unsafe-call,@typescript-eslint/no-unsafe-member-access
ReactModal.setAppElement('#__next');

type Props = {
    isOpen: boolean;
    closeModal: () => void;
    title: string;
};

const Modal = ({
    isOpen,
    closeModal,
    title,
    children,
}: PropsWithChildren<Props>): React.ReactElement => (
    <ReactModal isOpen={isOpen} onRequestClose={closeModal} className={cn['modal-wrapper']}>
        <div className={cn['modal-wrapper__window']}>
            <div className={cn['modal-wrapper__title']}>
                <span className={cn['modal-wrapper__header3']}>{title}</span>
                <ModalCloseIcon className={cn['modal-wrapper__close']} onClick={closeModal} />
            </div>
            {children}
        </div>
    </ReactModal>
);

export default Modal;
