// Импорт стилей
import React from 'react';

// Импорт компонентов
import Modal from '@/components/blocks/Modal';

import CartForm from './modules/CartForm';
import CartItems from './modules/CartItems';

type Props = {
    isOpen: boolean;
    onClose: () => void;
};

export const Cart = ({ isOpen, onClose }: Props): React.ReactElement => (
    <Modal isOpen={isOpen} closeModal={onClose} title="Ваш заказ">
        <CartItems />
        <CartForm onClose={onClose} />
    </Modal>
);
