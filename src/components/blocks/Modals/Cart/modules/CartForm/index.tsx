import React from 'react';
import { useForm } from 'react-hook-form';

import ButtonComponent from '@/components/UI/buttonComponent';
import { withStateManager } from '@/hock/withStateManager';
import { IStore } from '@/mobx/store';

// Импорт стилей
import cn from './styles.module.sass';

type FormData = {
    name: string;
    phone: string;
    address: string;
    payment: '' | 'courier' | 'online';
};

type InjectedProps = Pick<IStore, 'cart'>;

type Props = {
    onClose: () => void;
};

const CartForm = ({ onClose, cart }: Props & InjectedProps): React.ReactElement => {
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm<FormData>({
        defaultValues: {
            payment: '',
        },
    });

    const onSubmit = (data: FormData) => {
        console.log('FormData: ', data, 'Items: ', cart.items);
        cart.clearCart();
        onClose();
    };

    return (
        <form className={cn.form} onSubmit={handleSubmit(onSubmit)}>
            <span className={cn['default-text']}>Контакты</span>
            <div className={cn['form-contacts']}>
                <div className={cn['form-contacts__name']}>
                    <input
                        className={cn['form-input']}
                        placeholder="Ваше имя"
                        {...register('name', {
                            required: true,
                            pattern: /^[A-Za-zА-Яа-яЁё]+$/i,
                        })}
                    />
                    {errors.name && (
                        <span className={cn['form-input__errors']}>
                            Имя должно состоять из букв!{errors.name.message}
                        </span>
                    )}
                </div>

                <div className={cn['form-contacts__phone']}>
                    <input
                        className={cn['form-input']}
                        placeholder="Телефон"
                        {...register('phone', {
                            required: true,
                            pattern: /^[0-9]+(\.[0-9]{1,2})?$/,
                        })}
                    />
                    {errors.phone && (
                        <span className={cn['form-input__errors']}>
                            Номер должен состоять из цифр!{errors.phone.message}
                        </span>
                    )}
                </div>

                <div className={cn['form-contacts__address']}>
                    <input
                        className={cn['form-input']}
                        placeholder="Адрес доставки"
                        {...register('address', {
                            required: {
                                value: true,
                                message: 'Поле обязательно для заполнения!',
                            },
                        })}
                    />
                    {errors.address && (
                        <span className={cn['form-input__errors']}>{errors.address.message}</span>
                    )}
                </div>
            </div>
            <span className={cn['default-text']}>Способ оплаты</span>
            <div className={cn['form-payment']}>
                <label className={cn['form-payment__container']} htmlFor="courier">
                    <input
                        className={cn['form-payment__radiobutton']}
                        id="courier"
                        type="radio"
                        value="courier"
                        {...register('payment', {
                            required: {
                                value: true,
                                message: 'Поле обязательно для заполнения!',
                            },
                        })}
                    />
                    <span className={cn['form-payment__text']}>
                        Оплата наличными или картой курьеру
                    </span>
                </label>

                <label className={cn['form-payment__container']} htmlFor="offline">
                    <input
                        className={cn['form-payment__radiobutton']}
                        id="offline"
                        type="radio"
                        value="offline"
                        {...register('payment', {
                            required: {
                                value: true,
                                message: 'Выберите тип оплаты!',
                            },
                        })}
                    />
                    <span className={cn['form-payment__text']}>Оплата картой онлайн на сайте</span>
                </label>
                {errors.payment && (
                    <span className={cn['form-payment__errors']}>{errors.payment.message}</span>
                )}
            </div>

            <ButtonComponent classname={cn['form-button']} type="submit">
                Оформить заказ
            </ButtonComponent>

            <span className={cn['form-description']}>
                Нажимая кнопку «Оформить заказ» вы соглашаетесь с политикой конфиденциальности
            </span>
        </form>
    );
};

export default withStateManager<Props, InjectedProps>(CartForm, stores => ({
    cart: stores.cart,
}));
