import Image from 'next/image';
import React from 'react';

import FormDeletePizzaIcon from '@/components/UI/svgComponents/FormDeletePizzaIcon';
import FormMinusPizzaIcon from '@/components/UI/svgComponents/FormMinusPizzaIcon';
import FormPlusPizzaIcon from '@/components/UI/svgComponents/FormPlusPizzaIcon';
import { withStateManager } from '@/hock/withStateManager';
import { IStore } from '@/mobx/store';

// Импорт стилей
import cn from './styles.module.sass';

type InjectedProps = Pick<IStore, 'cart'>;

const CartItems = ({ cart }: InjectedProps): React.ReactElement => (
    <div className={cn['carts-container']}>
        <ul className={cn['carts-container__list']}>
            {cart.items.map(item => (
                <div className={cn['carts-container__item']} key={`${item.pizza.id}_${item.size}`}>
                    <div className={cn['carts-container__info']}>
                        <div className={cn['carts-container__card__image']}>
                            {item.pizza.types.map(({ Icon, id }) => (
                                <Icon key={id} width={12} height={12} />
                            ))}
                        </div>

                        <div className={cn['carts-container__card__size']}>
                            <Image src={item.pizza.image} alt={item.pizza.name} layout="fill" />
                        </div>

                        <div className={cn['carts-container__info__title']}>
                            <span className={cn['default-text']}>{item.pizza.name}</span>
                            <span className={cn['default-description']}>{item.size} см</span>
                        </div>
                    </div>

                    <div className={cn['carts-container__inner']}>
                        <div className={cn['carts-container__target']}>
                            <button
                                type="button"
                                className={cn['carts-container__target__delete']}
                                disabled={item.count === 1}
                                onClick={() => cart.decreaseCount(item.pizza.id, item.size)}
                            >
                                <FormMinusPizzaIcon />
                            </button>

                            <span className={cn['carts-container__count']}>{item.count}</span>

                            <button
                                type="button"
                                className={cn['carts-container__target__add']}
                                onClick={() => cart.increaseCount(item.pizza.id, item.size)}
                            >
                                <FormPlusPizzaIcon />
                            </button>
                        </div>
                        <span className={cn['carts-container__item__summary']}>
                            {' '}
                            {item.count * item.price} руб
                        </span>
                    </div>
                    <span
                        role="menuitem"
                        tabIndex={0}
                        onClick={() => cart.removeFromCart(item.pizza.id, item.size)}
                        onKeyUp={() => cart.removeFromCart(item.pizza.id, item.size)}
                        onKeyDown={() => cart.removeFromCart(item.pizza.id, item.size)}
                        onFocus={() => cart.removeFromCart(item.pizza.id, item.size)}
                    >
                        <FormDeletePizzaIcon
                            width={10}
                            height={10}
                            className={cn['carts-container__item__delete']}
                        />
                    </span>
                </div>
            ))}
        </ul>

        <div className={cn['carts-container__summary']}>
            <span className={cn['default-text']}>
                Сумма заказа:
                <span className={cn['carts-container__summary__payment']}>
                    {cart.getTotalAmount()}
                </span>{' '}
                руб
            </span>
        </div>
    </div>
);

export default withStateManager<unknown, InjectedProps>(CartItems, stores => ({
    cart: stores.cart,
}));
