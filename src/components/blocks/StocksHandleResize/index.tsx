import React, { useEffect, useState } from 'react';

import MainStocks from '../../layouts/Main/modules/MainStocks';
// Импорт Компонентов
import StocksSlider from '../StocksSlider';

type TState = {
    width: number | undefined;
    height: number | undefined;
};

const StocksHandleResize = (): React.ReactElement => {
    const [windowSize, setWindowSize] = useState<TState>({
        width: undefined,
        height: undefined,
    });

    useEffect(() => {
        function handleResize() {
            setWindowSize({
                width: window.innerWidth,
                height: window.innerHeight,
            });
        }

        window.addEventListener('resize', handleResize);

        handleResize();

        return () => window.removeEventListener('resize', handleResize);
    }, []);

    return windowSize.width && windowSize.width < 750 ? <StocksSlider /> : <MainStocks />;
};

export default StocksHandleResize;
